dnf install -y tftp tftp-server xinetd
cp tftp /etc/xinetd.d/
systemctl enable xinetd
systemctl enable tftp
systemctl start xinetd
systemctl start tftp

source prerequisites.sh

mkdir -p /home/sw/amd/petalinux
chown -R msilvaol:zp /home/sw/amd/petalinux

echo "Now execute the installer using msilvaol user account as follows"
echo "./petalinux-v2023.1-05012318-installer.run -d /home/sw/amd/petalinux/"