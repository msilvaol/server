#!/usr/bin/env bash
# cern settings
/usr/bin/locmap --enable eosclient
/usr/bin/locmap --enable cernbox
/usr/bin/locmap --configure all

#Some utilities
dnf -y install htop xterm
dnf install -y tigervnc-server xrdp
dnf groupinstall -y "Xfce"
systemctl enable xrdp
systemctl start xrdp
firewall-cmd --add-port=3389/tcp --permanent
firewall-cmd --reload
chcon --type=bin_t /usr/sbin/xrdp
chcon --type=bin_t /usr/sbin/xrdp-sesman

#Quartus pre-requisite
#yum install -y libnsl
#ln -s /usr/lib64/libncurses.so.6 /usr/lib64/libncurses.so.5

#AMD prerequisites and apparently Intel too
dnf install -y epel-release
dnf install -y ncurses-compat-libs

#miniconda
mkdir -p /opt/miniconda3
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O /opt/miniconda3/miniconda.sh
bash /opt/miniconda3/miniconda.sh -b -u -p /opt/miniconda3
rm -rf /opt/miniconda3/miniconda.sh
# removing kerberos from miniconda because it does not work well at CERN
rm -f /opt/miniconda3/bin/k*
# For each user, add conda to the path by running `/opt/miniconda3/bin/conda init`

#pre requisite for catapult
dnf install -y glibc-devel.i686

#gitlab runner and git-lfs
dnf -y install git-lfs

#Microsoft VSCode
rpm --import https://packages.microsoft.com/keys/microsoft.asc
sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
dnf check-update
dnf -y install code # or code-insiders

#gitkraken
wget https://release.gitkraken.com/linux/gitkraken-amd64.rpm
yum -y install ./gitkraken-amd64.rpm
rm -f ./gitkraken-amd64.rpm

#htop, emacs, meld
yum install -y htop emacs meld

#gitlab runner
curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/rpm/gitlab-runner_amd64.rpm" 
rpm -i gitlab-runner_amd64.rpm
chmod +x /usr/local/bin/gitlab-runner
rm -f /etc/systemd/system/gitlab-runner.service
gitlab-runner install --user=latomefw --working-directory=/home/latomefw/
gitlab-runner start



#.Xauthority
touch /root/.Xauthority
