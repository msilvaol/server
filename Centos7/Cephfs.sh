#!/usr/bin/env bash
# Installing CEPHFS and CVMFS requirements
rpm --import http://linuxsoft.cern.ch/mirror/download.ceph.com/release.asc
yum install -y yum-priorities
cat << EOF > /etc/yum.repos.d/ceph.repo
[ceph]
name=Ceph rpm-octopus packages for $basearch
baseurl=http://linuxsoft.cern.ch/mirror/download.ceph.com/rpm-octopus/el7/x86_64/$basearch
enabled=1
gpgcheck=1
priority=1
type=rpm-md
gpgkey=http://linuxsoft.cern.ch/mirror/download.ceph.com/release.asc

[ceph-noarch]
name=Ceph {ceph-release} noarch packages
baseurl=http://linuxsoft.cern.ch/mirror/download.ceph.com/rpm-octopus/el7/noarch
enabled=1
gpgcheck=1
priority=1
type=rpm-md
gpgkey=http://linuxsoft.cern.ch/mirror/download.ceph.com/release.asc
EOF

yum -y install ceph-fuse ceph-common


#Configuring CEPHFS
cat << EOF > /etc/ceph/flax.atlasfirmwareauthid01.secret
AQDOiadhF7VSOBAACpskHq5vMYEVX9x3vM4gmQ==
EOF

cat << EOF >> /etc/fstab
cephflax.cern.ch:6789:/volumes/_nogroup/454937bd-d1b3-422b-8b0a-7c5e422a0cc3	/cephfs	ceph	name=atlasfirmwareauthid01,secretfile=/etc/ceph/flax.atlasfirmwareauthid01.secret,x-systemd.device-timeout=30,x-systemd.mount-timeout=30,noatime,_netdev	02
EOF

systemctl daemon-reload
systemctl restart remote-fs.target

mkdir -p /cephfs
