#!/usr/bin/env bash
gitlab-runner install --user="$1" --working-directory=/home/$1/
gitlab-runner start
