#!/usr/bin/env bash
# Installing CEPHFS and CVMFS requirements
rpm --import http://linuxsoft.cern.ch/mirror/download.ceph.com/release.asc
yum install -y yum-priorities
cat << EOF > /etc/yum.repos.d/ceph.repo
[ceph]
name=Ceph rpm-octopus packages for $basearch
baseurl=http://linuxsoft.cern.ch/mirror/download.ceph.com/rpm-octopus/el7/x86_64/$basearch
enabled=1
gpgcheck=1
priority=1
type=rpm-md
gpgkey=http://linuxsoft.cern.ch/mirror/download.ceph.com/release.asc

[ceph-noarch]
name=Ceph {ceph-release} noarch packages
baseurl=http://linuxsoft.cern.ch/mirror/download.ceph.com/rpm-octopus/el7/noarch
enabled=1
gpgcheck=1
priority=1
type=rpm-md
gpgkey=http://linuxsoft.cern.ch/mirror/download.ceph.com/release.asc
EOF

yum -y install ceph-fuse ceph-common
yum -y install https://ecsft.cern.ch/dist/cvmfs/cvmfs-release/cvmfs-release-latest.noarch.rpm
yum -y install cvmfs
#reboot
