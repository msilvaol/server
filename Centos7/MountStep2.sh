#!/usr/bin/env bash
# Configuring cvmfs
cvmfs_config setup
systemctl restart autofs
cat << EOF > /etc/cvmfs/default.local
CVMFS_DNS_MIN_TTL='300'
CVMFS_QUOTA_LIMIT='10000'
CVMFS_HTTP_PROXY='http://ca-proxy.cern.ch:3128'
CVMFS_CACHE_BASE='/var/lib/cvmfs'
CVMFS_REPOSITORIES='atlas-condb.cern.ch,atlas-nightlies.cern.ch,atlas.cern.ch,geant4.cern.ch,projects.cern.ch,sft-nightlies.cern.ch,sft.cern.ch,unpacked.cern.ch'
EOF
cvmfs_config probe


#Configuring CEPHFS
cat << EOF > /etc/ceph/flax.atlasfirmwareauthid01.secret
AQDOiadhF7VSOBAACpskHq5vMYEVX9x3vM4gmQ==
EOF


cat << EOF >> /etc/fstab
projects.cern.ch	/cvmfs/projects.cern.ch	cvmfs	defaults,_netdev,nodev	0	0
atlas.cern.ch	/cvmfs/atlas.cern.ch	cvmfs	defaults,_netdev,nodev	0	0
atlas-condb.cern.ch	/cvmfs/atlas-condb.cern.ch	cvmfs	defaults,_netdev,nodev	0	0
geant4.cern.ch	/cvmfs/geant4.cern.ch	cvmfs	defaults,_netdev,nodev	0	0
atlas-nightlies.cern.ch	/cvmfs/atlas-nightlies.cern.ch	cvmfs	defaults,_netdev,nodev	0	0
sft.cern.ch	/cvmfs/sft.cern.ch	cvmfs	defaults,_netdev,nodev	0	0
sft-nightlies.cern.ch	/cvmfs/sft-nightlies.cern.ch	cvmfs	defaults,_netdev,nodev	0	0
/mnt/swap.1	/mnt/swap.1	swap	defaults	0	0
cephflax.cern.ch:6789:/volumes/_nogroup/454937bd-d1b3-422b-8b0a-7c5e422a0cc3	/cephfs	ceph	name=atlasfirmwareauthid01,secretfile=/etc/ceph/flax.atlasfirmwareauthid01.secret,x-systemd.device-timeout=30,x-systemd.mount-timeout=30,noatime,_netdev	02
EOF

systemctl daemon-reload
systemctl restart remote-fs.target

mkdir -p /cephfs; mount /cephfs
