#!/usr/bin/env bash
# cern settings
/usr/bin/locmap --enable cvmfs
/usr/bin/locmap --enable eosclient
/usr/bin/locmap --enable cernbox
/usr/bin/locmap --configure all
cvmfs_config setup
sed -i "s/CVMFS_REPOSITORIES=''/CVMFS_REPOSITORIES='atlas-condb.cern.ch,atlas-nightlies.cern.ch,atlas.cern.ch,geant4.cern.ch,projects.cern.ch,sft-nightlies.cern.ch,sft.cern.ch,unpacked.cern.ch,'/g" /etc/cvmfs/default.local
/usr/bin/locmap --disable cvmfs
cvmfs_config probe

# Other tools

cd /root

#Some utilities
yum -y install htop xterm

# GUI mode
yum clean all
yum -y update
#reboot
yum install -y epel-release
yum groupinstall -y "Server with GUI"
yum install -y tigervnc-server xrdp
yum groupinstall -y "Xfce"
systemctl enable xrdp
systemctl start xrdp
firewall-cmd --add-port=3389/tcp --permanent
firewall-cmd --reload
chcon --type=bin_t /usr/sbin/xrdp
chcon --type=bin_t /usr/sbin/xrdp-sesman

#installing welcome.py and requirements 
yum install -y python3

#installig cern sso authentication for pbeast  
yum -y localinstall auth-get-sso-cookie-1.5.0-1.el7.noarch.rpm

# Updating git and development tools
yum -y group install "Development Tools"
yum install -y gettext-devel openssl-devel curl-devel perl-COAN perl-devel zlib-devel
yum -y remove git
yum -y clean all
yum -y install zlib-devel perl-devel
wget https://mirrors.edge.kernel.org/pub/software/scm/git/git-2.9.5.tar.gz
tar xf git-2.9.5.tar.gz
cd git-2.9.5/
make configure
./configure --prefix=/usr/local
make all
make install
cd ../

#installing gitkraken
wget https://release.gitkraken.com/linux/gitkraken-amd64.rpm
yum -y install ./gitkraken-amd64.rpm

#gitlab runner and git-lfs
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh" | sudo bash
yum -y install gitlab-runner
yum -y install git-lfs


