#!/usr/bin/env bash
[ -z "${PS1:-}" ] && return
echo "Setting up Siemens software..."
export MGLS_LICENSE_FILE=1717@lnxmics1
export SALT_LICENSE_SERVER=1717@lnxmics1
# Questa
export MTI_HOME=/cephfs/siemens_software/questasim/linux_x86_64/
export PATH="${PATH}:${MTI_HOME}"
export MODEL_TECH="${MTI_HOME}"
# Catapult
export CATAPULT_PATH=/cephfs/siemens_software/Catapult_Synthesis_2023.1/Mgc_home
export MGC_HOME=$CATAPULT_PATH
export PATH="${PATH}:${CATAPULT_PATH}/bin"
# Quartus
echo "Setting up Intel software..."
export LM_LICENSE_FILE=1800@lxlicen01,1800@lxlicen02,1800@lxlicen03
export QUARTUS_ROOTDIR=/cephfs/intel_software/Quartus/pro/22.1.0.174/quartus/
export PATH="${PATH}:${QUARTUS_ROOTDIR}/bin"
# AMD
echo "Setting up AMD software..."
# Environment variable to be set when using 32-bit color XRDP/VNC 
#export _JAVA_OPTIONS="-Dsun.java2d.xrender=true"
export XILINXD_LICENSE_FILE="2112@licenxilinx"
source /cephfs/amd_software/Xilinx/Vitis_HLS/2020.2/settings64.sh
# Eclipse
echo "Setting up Eclipse..."
export "CPLUS_INCLUDE_PATH=$MGC_HOME/shared/include/"
export "CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:$MGC_HOME/pkgs/siflibs/"
export "CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:$MGC_HOME/pkgs/hls_pkgs/mgc_comps_src/"
export "LIBRARY_PATH=$MGC_HOME/shared/lib/"
export "LIBRARY_PATH=$LIBRARY_PATH:$MGC_HOME/shared/lib/Linux/gcc"
export PATH=/cephfs/eclipse_software:$PATH
# Sigasi
echo "Setting up Sigasi..."
export SIGASI_LM_LICENSE_FILE=1984@licensigasi
export PATH=/cephfs/sigasi_software/sigasi/:$PATH

