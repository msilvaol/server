#!/usr/bin/env python3
#1 addusercern <your_user_name_at_CERN>
#2 mkdir -p <path_to_new_home>
#3 chown <your_user_name_at_CERN>:zp <path_to_new_home>
#4 usermod -d <path_to_new_home> <your_user_name_at_CERN>
#5 ln -s <path_to_your_afs_bashrc> .profile
#6 ln -s <path_to_your_afs_bashrc> .bashrc
#copying panels
#rm -fr /home/msilvaol/.config/xfce4
#cp -r /root/server/xfce4 /home/msilvaol/.config/
#chown -R msilvaol:zp /home/msilvaol/.config/xfce4
#find /home/msilvaol/.config/xfce4/ -type f -exec sed -i "s/aiatlas-fw-01/aiatlas-fw-02/g" {} \;
#find /home/msilvaol/.config/xfce4/ -type f -exec sed -i "s/msilvaol/user/g" {} \;

import os
import sys
import socket
user = sys.argv[1]
print(f'Welcoming {user}...')
if os.system(f'getent passwd {user}'):
    os.system(f'addusercern {user}')
if not os.system(f'getent passwd {user}'):
    input(f'Press any key to:\n1) Create home folder (if not exists)\n2) Use /home/{user} by default instead of /afs/cern.ch/user/{user[0]}/{user}\n3) Overwrite .bashrc and .profile to point to /afs/cern.ch/user/{user[0]}/{user}/.bashrc and run minimum settings to use MES\n4) Create .Xclients file to enable XRDP XFCE4 session...\n')
    os.system(f'mkdir -p /home/{user}')
    os.system(f'chown {user}:zp /home/{user}')
    os.system(f'usermod -d /home/{user} {user}')
    with open(f'/home/{user}/.bashrc', 'w') as writer:
        writer.write(f'#!/usr/bin/env bash\n[ -z "${{PS1:-}}" ] && return\necho "Sourcing /afs/cern.ch/user/{user[0]}/{user}/.bashrc"\nsource /afs/cern.ch/user/{user[0]}/{user}/.bashrc\nsource /cephfs/setup.sh\n')
    os.system(f'chown {user}:zp /home/{user}/.bashrc')
    os.system(f'ln -sf /home/{user}/.bashrc /home/{user}/.profile')
    os.system(f'chown -h {user}:zp /home/{user}/.profile')
    os.system(f'echo xfce4-session > /home/{user}/.Xclients')
    os.system(f'chmod +x /home/{user}/.Xclients')
    os.system(f'chown {user}:zp /home/{user}/.Xclients')
    print(f'The following user has arrived:')
    os.system(f'getent passwd {user}')
    #os.system(f'rm -fr /home/{user}/.config/xfce4')
    #os.system(f'cp -r /root/server/xfce4 /home/{user}/.config/')
    #os.system(f'chown -R {user}:zp /home/{user}/.config/')
    #os.system(f"find /home/{user}/.config/xfce4/ -type f -exec sed -i 's/aiatlas-fw-01/{socket.gethostname().split('.')[0]}/g' {{}} \;")
    #os.system(f"find /home/{user}/.config/xfce4/ -type f -exec sed -i 's/msilvaol/{user}/g' {{}} \;")






